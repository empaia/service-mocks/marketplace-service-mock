import copy
from uuid import uuid4

import requests

from marketplace_service_mock.models.marketplace.app import ApiVersion
from tests.singletons import settings

app_valid_global_config = {
    "private_string_global": "some str",
    "private_int_global": 100,
    "private_bool_global": True,
    "private_float_global": 3.5,
}

app_valid_customer_config = {
    "private_string_customer": "some str",
    "private_int_customer": 100,
    "private_bool_customer": False,
    "private_float_customer": 1.522,
}

app_config = {
    "app_id": "dummy",
    "global": app_valid_global_config,
    "customer": app_valid_global_config,
}

app_ui_config = {"csp": {"style_src": {"unsafe_inline": True}, "font_src": {"unsafe_inline": True}}}


def create_portal_app():
    portal_app_id = str(uuid4())
    organization_id = str(uuid4())
    app_view_v1_id = str(uuid4())
    app_view_v2_id = str(uuid4())
    app_view_v3_id = str(uuid4())
    app_v1_id = str(uuid4())
    app_v2_id = str(uuid4())
    app_v3_id = str(uuid4())
    user_id = str(uuid4())

    portal_app_json = {
        "id": portal_app_id,
        "organization_id": organization_id,
        "status": "DRAFT",
        "active_app_views": {
            "v1": {
                "version": "v1.2",
                "details": {
                    "name": "PD-L1 Quantifier",
                    "marketplace_url": "http://url.to/store",
                    "description": [{"lang": "EN", "text": "Some text"}],
                },
                "media": {"peek": [], "banner": [], "workflow": [], "manual": []},
                "tags": {"tissues": [], "stains": [], "indications": [], "analysis": [], "clearances": []},
                "non_functional": False,
                "research_only": True,
                "api_version": "v1",
                "created_at": "1598611645",
                "reviewed_at": "1598611645",
                "id": app_view_v1_id,
                "portal_app_id": portal_app_id,
                "organization_id": organization_id,
                "status": "DRAFT",
                "app": {
                    "ead": {
                        "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
                        "name": "Tutorial App 01 v1",
                        "name_short": "TA01v1",
                        "namespace": "org.empaia.vendor_name.tutorial_app_01.v1",
                        "description": "Human readable description",
                        "configuration": {
                            "private_string_global": {"type": "string", "optional": False, "storage": "global"},
                            "private_int_global": {"type": "integer", "optional": False, "storage": "global"},
                            "private_bool_global": {"type": "bool", "optional": False, "storage": "global"},
                            "private_float_global": {"type": "float", "optional": False, "storage": "global"},
                        },
                        "inputs": {
                            "my_wsi": {"type": "wsi"},
                            "my_rectangle": {"type": "rectangle", "reference": "inputs.my_wsi"},
                        },
                        "outputs": {"tumor_cell_count": {"type": "integer"}},
                    },
                    "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_xyz",
                    "app_ui_url": "http://app1.empaia.org",
                    "app_ui_configuration": {"csp": None},
                    "app_documentation_url": None,
                    "id": app_v1_id,
                    "version": "v1.2",
                    "has_frontend": True,
                    "status": "DRAFT",
                    "portal_app_id": portal_app_id,
                    "creator_id": user_id,
                    "created_at": "1598611645",
                    "updated_at": "1598611645",
                },
                "creator_id": user_id,
                "review_comment": "Review comment",
                "reviewer_id": user_id,
            },
            "v2": {
                "version": "v1.2",
                "details": {
                    "name": "PD-L1 Quantifier",
                    "marketplace_url": "http://url.to/store",
                    "description": [{"lang": "EN", "text": "Some text"}],
                },
                "media": {"peek": [], "banner": [], "workflow": [], "manual": []},
                "tags": {"tissues": [], "stains": [], "indications": [], "analysis": []},
                "non_functional": False,
                "research_only": False,
                "api_version": "v2",
                "created_at": "1598611645",
                "reviewed_at": "1598611645",
                "id": app_view_v2_id,
                "portal_app_id": portal_app_id,
                "organization_id": organization_id,
                "status": "DRAFT",
                "app": {
                    "ead": {
                        "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
                        "name": "Tutorial App 01 v2",
                        "name_short": "TA01v2",
                        "namespace": "org.empaia.vendor_name.tutorial_app_01.v2",
                        "description": "Human readable description",
                        "configuration": {
                            "private_string_global": {"type": "string", "optional": False, "storage": "global"},
                            "private_int_global": {"type": "integer", "optional": False, "storage": "global"},
                            "private_bool_global": {"type": "bool", "optional": False, "storage": "global"},
                            "private_float_global": {"type": "float", "optional": False, "storage": "global"},
                        },
                        "inputs": {
                            "my_wsi": {"type": "wsi"},
                            "my_rectangle": {"type": "rectangle", "reference": "inputs.my_wsi"},
                        },
                        "outputs": {"tumor_cell_count": {"type": "integer"}},
                    },
                    "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_xyz",
                    "app_ui_url": "http://app1.empaia.org",
                    "app_ui_configuration": {"csp": None},
                    "app_documentation_url": None,
                    "id": app_v2_id,
                    "version": "v1.2",
                    "has_frontend": True,
                    "status": "DRAFT",
                    "portal_app_id": portal_app_id,
                    "creator_id": user_id,
                    "created_at": "1598611645",
                    "updated_at": "1598611645",
                },
                "creator_id": user_id,
                "review_comment": "Review comment",
                "reviewer_id": user_id,
            },
            "v3": {
                "version": "v1.2",
                "details": {
                    "name": "PD-L1 Quantifier",
                    "marketplace_url": "http://url.to/store",
                    "description": [{"lang": "EN", "text": "Some text"}],
                },
                "media": {"peek": [], "banner": [], "workflow": [], "manual": []},
                "tags": {"tissues": [], "stains": [], "indications": [], "analysis": []},
                "non_functional": False,
                "api_version": "v3",
                "created_at": "1598611645",
                "reviewed_at": "1598611645",
                "id": app_view_v3_id,
                "portal_app_id": portal_app_id,
                "organization_id": organization_id,
                "status": "DRAFT",
                "app": {
                    "ead": {
                        "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
                        "name": "Tutorial App 01 v3",
                        "name_short": "TA01v3",
                        "namespace": "org.empaia.vendor_name.tutorial_app_01.v3.0",
                        "description": "Human readable description",
                        "configuration": {
                            "global": {
                                "private_string_global": {"type": "string", "optional": False},
                                "private_int_global": {"type": "integer", "optional": False},
                                "private_bool_global": {"type": "bool", "optional": False},
                                "private_float_global": {"type": "float", "optional": False},
                            },
                            "customer": {
                                "private_string_customer": {"type": "string", "optional": False},
                                "private_int_customer": {"type": "integer", "optional": False},
                                "private_bool_customer": {"type": "bool", "optional": False},
                                "private_float_customer": {"type": "float", "optional": False},
                            },
                        },
                        "io": {
                            "my_wsi": {"type": "wsi"},
                            "my_rectangle": {"type": "rectangle", "reference": "io.my_wsi"},
                            "tumor_cell_count": {"type": "integer"},
                        },
                        "modes": {
                            "standalone": {"inputs": ["my_wsi", "my_rectangle"], "outputs": ["tumor_cell_count"]}
                        },
                    },
                    "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_xyz",
                    "app_ui_url": "http://app1.empaia.org",
                    "app_ui_configuration": {"csp": None},
                    "app_documentation_url": None,
                    "id": app_v3_id,
                    "version": "v1.2",
                    "has_frontend": True,
                    "status": "DRAFT",
                    "portal_app_id": portal_app_id,
                    "creator_id": user_id,
                    "created_at": "1598611645",
                    "updated_at": "1598611645",
                },
                "creator_id": user_id,
                "review_comment": "Review comment",
                "reviewer_id": user_id,
            },
        },
        "creator_id": user_id,
        "created_at": "1598611645",
        "updated_at": "1598611645",
    }
    return portal_app_id, organization_id, [app_v1_id, app_v2_id, app_v3_id], copy.deepcopy(portal_app_json)


def test_get_tags():
    r = requests.get(f"{settings.mpsm_url}/v1/public/tags")
    resp = r.json()
    assert r.status_code == 200
    assert "tissues" in resp
    assert len(resp["tissues"]) == 44
    assert "stains" in resp
    assert len(resp["stains"]) == 197
    assert "analysis" in resp
    assert len(resp["analysis"]) == 8
    assert "indications" in resp
    assert len(resp["indications"]) == 65
    assert "clearances" in resp
    assert len(resp["clearances"]) == 9


def test_post_portal_app():
    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"

    # correct
    _, _, _, portal_app = create_portal_app()
    r = requests.post(url, json=portal_app)
    print(r.json())
    assert r.status_code == 200

    # only v1, v2
    _, _, _, portal_app = create_portal_app()
    portal_app["active_app_views"]["v3"] = None
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    # bad ead schema
    _, _, _, portal_app = create_portal_app()
    portal_app["active_app_views"]["v2"]["app"]["ead"]["inputs"]["my_wsi"]["type"] = None
    r = requests.post(url, json=portal_app)
    assert r.status_code == 422

    # bad ead semantic
    _, _, _, portal_app = create_portal_app()
    portal_app["active_app_views"]["v2"]["app"]["ead"]["inputs"]["my_rectangle"]["reference"] = [
        "inputs.NON_EXISTENT_IDENTIFIER"
    ]
    r = requests.post(url, json=portal_app)
    assert r.status_code == 422


def test_delete_portal_app_by_portal_app_id():
    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"

    portal_app_id, _, _, portal_app = create_portal_app()
    r = requests.post(url, json=portal_app)
    print(r.json())
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps/{portal_app_id}"
    r = requests.delete(url)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/portal-apps/{portal_app_id}"
    headers = {"organization-id": "dummy_org"}
    r = requests.get(url, headers=headers)
    assert r.status_code == 404


def test_delete_portal_app_by_app_id():
    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"

    portal_app_id, _, app_ids, portal_app = create_portal_app()
    app_id_v1 = app_ids[0]
    r = requests.post(url, json=portal_app)
    print(r.json())
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps/by_app/{app_id_v1}"
    r = requests.delete(url)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/portal-apps/{portal_app_id}"
    headers = {"organization-id": "dummy_org"}
    r = requests.get(url, headers=headers)
    assert r.status_code == 404


def test_get_portal_apps():
    url = f"{settings.mpsm_url}/v1/customer/portal-apps/query"
    headers = {"organization-id": "dummy_org"}
    r = requests.put(url, json={}, headers=headers)
    assert r.status_code == 200
    portal_app_count = r.json()["item_count"]

    _, _, _, portal_app = create_portal_app()
    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/portal-apps/query"
    headers = {"organization-id": "dummy_org"}
    r = requests.put(url, json={}, headers=headers)
    assert r.status_code == 200
    assert r.json()["item_count"] == portal_app_count + 1
    assert len(r.json()["items"]) == portal_app_count + 1


def test_get_portal_app_by_id():
    portal_app_id, _, _, portal_app = create_portal_app()

    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/portal-apps/{portal_app_id}"
    headers = {"organization-id": "dummy_org"}
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    resp = r.json()
    assert resp["id"] == portal_app_id
    del resp["active_app_views"]["v1"]["app"]["created_at"]
    del resp["active_app_views"]["v1"]["app"]["updated_at"]
    del portal_app["active_app_views"]["v1"]["app"]["created_at"]
    del portal_app["active_app_views"]["v1"]["app"]["updated_at"]
    assert resp["active_app_views"]["v2"]["app"]["app_ui_configuration"] == {
        "csp": None,
        "iframe": None,
        "tested": None,
    }
    del resp["active_app_views"]["v1"]["app"]["app_ui_configuration"]
    del portal_app["active_app_views"]["v1"]["app"]["app_ui_configuration"]
    assert resp["active_app_views"]["v1"]["app"] == portal_app["active_app_views"]["v1"]["app"]


def test_get_app_by_id():
    portal_app_id, _, app_ids, portal_app = create_portal_app()

    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    app_id_v2 = app_ids[1]

    url = f"{settings.mpsm_url}/v1/customer/apps/{app_id_v2}"
    headers = {"organization-id": "dummy_org"}
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    resp = r.json()
    print(resp)
    assert resp["portal_app_id"] == portal_app_id
    del resp["created_at"]
    del resp["updated_at"]
    del portal_app["active_app_views"]["v2"]["app"]["created_at"]
    del portal_app["active_app_views"]["v2"]["app"]["updated_at"]
    assert resp["app_ui_configuration"] == {"csp": None, "iframe": None, "tested": None}
    del resp["app_ui_configuration"]
    del portal_app["active_app_views"]["v2"]["app"]["app_ui_configuration"]
    assert resp == portal_app["active_app_views"]["v2"]["app"]


def test_update_app_ui_url():
    _, _, app_ids, portal_app = create_portal_app()
    app_id_v1 = app_ids[0]
    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    app_ui = {"app_ui_url": "https://UPDATED.URL"}
    url_update = f"{settings.mpsm_url}/v1/custom-mock/apps/{app_id_v1}/app-ui-url"
    r = requests.put(url_update, json=app_ui)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/apps/{app_id_v1}"
    headers = {"organization-id": "dummy_org"}
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    print(r.json())
    assert r.json()["app_ui_url"] == app_ui["app_ui_url"]
    assert r.json()["has_frontend"] == True


def test_post_app_global_config_v1():
    _, _, app_ids, portal_app = create_portal_app()
    app_id_v1 = app_ids[0]

    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    post_config = copy.deepcopy(app_valid_global_config)

    url = f"{settings.mpsm_url}/v1/custom-mock/apps/{app_id_v1}/config/global"
    r = requests.put(url, json=post_config)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/apps/{app_id_v1}/config"
    headers = {"organization-id": "dummy_org"}
    r = requests.get(url, headers=headers)
    global_config = r.json()["global"]
    assert r.status_code == 200
    assert post_config == global_config
    assert "'private_bool_global': 1" not in str(global_config)
    assert "'private_bool_global': True" in str(global_config)


def test_post_app_global_config_v3():
    _, organization_id, app_ids, portal_app = create_portal_app()
    app_id_v3 = app_ids[2]

    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    global_config_copy = copy.deepcopy(app_valid_global_config)

    url = f"{settings.mpsm_url}/v1/custom-mock/apps/{app_id_v3}/config/global"
    r = requests.put(url, json=global_config_copy)
    assert r.status_code == 200

    customer_config_copy = copy.deepcopy(app_valid_customer_config)

    url = f"{settings.mpsm_url}/v1/custom-mock/apps/{app_id_v3}/config/customer/{organization_id}"
    r = requests.put(url, json=customer_config_copy)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/apps/{app_id_v3}/config"
    headers = {"organization-id": portal_app["organization_id"]}
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    global_config = r.json()["global"]
    assert global_config_copy == global_config
    assert "'private_bool_global': 1" not in str(global_config)
    assert "'private_bool_global': True" in str(global_config)
    customer_config = r.json()["customer"]
    assert customer_config_copy == customer_config
    assert "'private_bool_customer': 0" not in str(customer_config)
    assert "'private_bool_customer': False" in str(customer_config)


def test_get_empty_app_config_v3():
    _, organization_id, app_ids, portal_app = create_portal_app()
    app_id_v3 = app_ids[2]

    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/apps/{app_id_v3}/config"
    headers = {"organization-id": organization_id}
    r = requests.get(url, headers=headers)
    print(r.json())
    assert r.status_code == 200
    assert {} == r.json()["global"]
    assert {} == r.json()["customer"]


def test_put_app_ui_config():
    ui_config_copy = copy.deepcopy(app_ui_config)
    app_id = str(uuid4())

    url = f"{settings.mpsm_url}/v1/custom-mock/apps/{app_id}/app-ui-config"
    r = requests.put(url, json=ui_config_copy)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/apps/{app_id}/app-ui-config"
    headers = {"organization-id": "dummy_org"}
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    ui_config_copy["csp"]["font_src"]["unsafe_eval"] = None
    ui_config_copy["csp"]["script_src"] = None
    ui_config_copy["csp"]["style_src"]["unsafe_eval"] = None
    ui_config_copy["iframe"] = None
    ui_config_copy["tested"] = None
    assert ui_config_copy == r.json()


def test_update_app_ui_config():
    ui_config_copy = copy.deepcopy(app_ui_config)
    app_id = str(uuid4())

    url = f"{settings.mpsm_url}/v1/custom-mock/apps/{app_id}/app-ui-config"
    r = requests.put(url, json=ui_config_copy)
    assert r.status_code == 200

    new_ui_config = {
        "csp": {"style_src": {"unsafe_inline": False}, "font_src": {"unsafe_inline": False}},
        "iframe": {"allow_popups": True},
        "tested": [{"browser": "firefox", "version": "103.2.34", "os": "win10"}],
    }

    url = f"{settings.mpsm_url}/v1/custom-mock/apps/{app_id}/app-ui-config"
    r = requests.put(url, json=new_ui_config)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/apps/{app_id}/app-ui-config"
    headers = {"organization-id": "dummy_org"}
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    resp = r.json()
    assert resp["csp"] is not None
    assert resp["csp"]["style_src"]["unsafe_inline"] is False
    assert resp["csp"]["font_src"]["unsafe_inline"] is False
    assert resp["iframe"] is not None
    assert resp["iframe"]["allow_popups"] is True
    assert resp["tested"] is not None
    assert len(resp["tested"]) == 1
    assert resp["tested"][0] == new_ui_config["tested"][0]


def test_query_app_view():
    _, _, app_ids, portal_app = create_portal_app()

    url = f"{settings.mpsm_url}/v1/custom-mock/portal-apps"
    r = requests.post(url, json=portal_app)
    assert r.status_code == 200

    url = f"{settings.mpsm_url}/v1/customer/app-views/query"
    headers = {"organization-id": "dummy_org"}
    query = {
        "apps": app_ids,
    }
    r = requests.put(url, json=query, headers=headers)
    assert r.status_code == 200
    resp = r.json()
    assert len(resp) == 3
    for app_view in resp:
        assert app_view["app"]["id"] in app_ids

    query = {"apps": app_ids, "api_versions": [ApiVersion.V1]}
    r = requests.put(url, json=query, headers=headers)
    assert r.status_code == 200
    assert len(r.json()) == 1
