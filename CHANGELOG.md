# Changelog

## 0.2.20

* update submodules for integration of structured reports to support EMP-0105

## 0.2.10 & 0.2.11

* renovate

## 0.2.8 & 0.2.9

* renovate

## 0.2.7

* renovate

## 0.2.6

* updated submodules for pixelmaps

## 0.2.5

* renovate

## 0.2.4

* renovate

## 0.2.3

* renovate

## 0.2.2

* changed order of strict datatypes in mps configuration model

## 0.2.1

* renovate

## 0.1.61 & 0.2.0

* migrate to pydantic v2
* renovate

## 0.1.60

* renovate

## 0.1.59

* renovate

## 0.1.58

* renovate

## 0.1.57

* renovate

## 0.1.56

* renovate

## 0.1.55

* renovate

## 0.1.54

* renovate

## 0.1.53

* renovate

## 0.1.52

* renovate

## 0.1.51

* renovate

## 0.1.50

* renovate

## 0.1.49

* updated models repo

## 0.1.48

* updated py_ead_validation and definitions repo

## 0.1.47

* added query endpoint for app views and additional field

## 0.1.46

* renovate

## 0.1.45

* update `has_frontend` in app-ui-url
* remove v0 routes

## 0.1.44

* renovate

## 0.1.43

* updated models repo (app configuration) and adapted mock

## 0.1.42

* adapt mock to real MPS and update models/definition/py_ead_validation

## 0.1.41

* renovate

## 0.1.40

* added ead validation to POST `v1/custom-mock/app`
* added `/tags` endpoint

## 0.1.39

* bugfix `v1/customer/apps/{app_id}/portal-app-preview`

## 0.1.38

* renovate

## 0.1.37

* updated models repo and mock

## 0.1.36

* renovate

## 0.1.35

* renovate

## 0.1.34

* updated models repo

## 0.1.33

* replaced `/apps/{app_id}/portal-app` endpoint with preview

## 0.1.32

* renovate

## 0.1.31

* renovate

## 0.1.30

* renovate

## 0.1.29

* renovate

## 0.1.28

* updated models repo

## 0.1.27

* renovate

## 0.1.26

* renovate

## 0.1.25

* renovate

## 0.1.24

* renovate

## 0.1.23

* renovate

## 0.1.22

* fixed str uuid comparision
* added minimal unit tests

## 0.1.21

* updated models repo

## 0.1.20

* added organization header to customer routes

## 0.1.19

* renovate

## 0.1.18

* renovate
* added app-ui config to v1

## 0.1.17

* renovate

## 0.1.16

* renovate

## 0.1.15

* adapted models to MPS

## 0.1.14

* renovate

## 0.1.13

* fix configuration type coercion

## 0.1.12

* renovate

## 0.1.11

* renovate

## 0.1.10

* renovate

## 0.1.9

* renovate

## 0.1.8

* renovate

## 0.1.7

* implemented v1 mock routes

## 0.1.6

* renovate

## 0.1.5

* updated dependencies

## 0.1.4

* updated dependencies

## 0.1.3

* updated ci

## 0.1.2

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

## 0.1.1

* mount vault routes at /api/v0 for backwards compatibility

## 0.1.0

* init
