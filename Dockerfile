FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.4.2@sha256:5c1a951baeed230dc36a9d36c3689faf7e639a3a2271eb4e64cc7660062d6f85 AS builder
COPY . /marketplace-service-mock
WORKDIR /marketplace-service-mock
RUN mkdir -p /data
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.3.1@sha256:54c07b7d87e7d70248fa590f4ed7e0217e2e11b76bb569724807acc82488197b
COPY --from=builder /marketplace-service-mock/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /marketplace-service-mock/dist /artifacts
COPY --chown=appuser:appuser --from=builder /data /data
RUN pip install /artifacts/*.whl
CMD uvicorn --workers=1 --host=0.0.0.0 --port=5000 marketplace_service_mock.app:app
