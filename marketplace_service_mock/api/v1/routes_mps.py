import json
from typing import List

from fastapi import Header, HTTPException
from pydantic import Field

from marketplace_service_mock.models.commons import RestrictedBaseModel
from marketplace_service_mock.models.marketplace.app import (
    AppConfiguration,
    AppUiConfiguration,
    ClosedApp,
    ClosedAppView,
    ClosedPortalApp,
    ClosedPortalAppList,
    ConfigurationModel,
    CustomerAppViewQuery,
    CustomerPortalAppQuery,
    TagList,
)
from marketplace_service_mock.singletons import (
    APP_V1_APP_UI_CONFIGURATION_BY_ID,
    APP_V1_CONFIGURATIONS_BY_ID,
    APPS_V1_BY_ID,
    api_integration,
    app_tags_model,
    app_ui_configurations_v1_file_path,
    apps_v1_file_path,
    configurations_v1_file_path,
    ead_validator,
    ead_validator_legacy,
)

from ...py_ead_validation.py_ead_validation.exceptions import (
    ConfigValidationError,
    EadSchemaValidationError,
    EadValidationError,
)


class AppUi(RestrictedBaseModel):
    app_ui_url: str = Field(example="htts://app.ui", description="App UI url")


def add_routes_mps(app):
    @app.get(
        "/public/tags",
        summary="Get all available tags",
        response_model=TagList,
        tags=["General"],
    )
    async def _(
        _=api_integration.global_depends(),
    ):
        return app_tags_model

    @app.post(
        "/custom-mock/portal-apps",
        summary="Post custom portal app",
        tags=["mps-custom-mock"],
    )
    async def _(
        portal_app: ClosedPortalApp,
        _=api_integration.global_depends(),
    ):
        portal_app_dict = portal_app.dict()
        for active_app_v in portal_app_dict["active_app_views"].keys():
            active_app = portal_app_dict["active_app_views"][active_app_v]
            if active_app:
                ead = active_app["app"]["ead"]
                _validate_ead(active_app_v, ead)
        APPS_V1_BY_ID[str(portal_app.id)] = portal_app.dict()
        with apps_v1_file_path.open("w") as f:
            json.dump(APPS_V1_BY_ID, f, default=str)
        return

    @app.put(
        "/custom-mock/apps/{app_id}/app-ui-url",
        summary="Add or update the global configuration of an existing app",
        tags=["mps-custom-mock"],
    )
    async def _(
        app_id: str,
        app_ui: AppUi,
        payload=api_integration.global_depends(),
    ):
        active_api_version, portal_app_id, _ = _get_app(app_id)
        APPS_V1_BY_ID[portal_app_id]["active_app_views"][active_api_version]["app"].update(
            {"app_ui_url": str(app_ui.app_ui_url), "has_frontend": True}
        )

        with apps_v1_file_path.open("w") as f:
            json.dump(APPS_V1_BY_ID, f, default=str)
        return

    @app.delete(
        "/custom-mock/portal-apps/{portal_app_id}",
        summary="Delete portal app by portal app ID",
        tags=["mps-custom-mock"],
    )
    async def _(
        portal_app_id: str,
        _=api_integration.global_depends(),
    ):
        if str(portal_app_id) in APPS_V1_BY_ID:
            del APPS_V1_BY_ID[portal_app_id]
            with apps_v1_file_path.open("w") as f:
                json.dump(APPS_V1_BY_ID, f, default=str)
            return
        raise HTTPException(status_code=404, detail=f"Portal app with ID {portal_app_id} not found")

    @app.delete(
        "/custom-mock/portal-apps/by_app/{app_id}",
        summary="Delete portal app by app ID",
        tags=["mps-custom-mock"],
    )
    async def _(
        app_id: str,
        _=api_integration.global_depends(),
    ):
        _, portal_app_id, _ = _get_app(app_id)
        if str(portal_app_id) in APPS_V1_BY_ID:
            del APPS_V1_BY_ID[portal_app_id]
            with apps_v1_file_path.open("w") as f:
                json.dump(APPS_V1_BY_ID, f, default=str)
            return
        raise HTTPException(status_code=404, detail=f"App with ID {app_id} not found")

    @app.put(
        "/customer/portal-apps/query",
        summary="Query portal apps",
        response_model=ClosedPortalAppList,
        tags=["mps-mock-customer"],
    )
    async def _(
        query: CustomerPortalAppQuery,
        skip: int = None,
        limit: int = None,
        organization_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        app_list = [APPS_V1_BY_ID[k] for k in APPS_V1_BY_ID]
        return {"item_count": len(app_list), "items": app_list}

    @app.get(
        "/customer/portal-apps/{portal_app_id}",
        summary="Get portal app by ID",
        response_model=ClosedPortalApp,
        tags=["mps-mock-customer"],
    )
    async def _(
        portal_app_id: str,
        organization_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        return _get_portal_app(portal_app_id=portal_app_id)

    @app.put(
        "/customer/app-views/query",
        summary="Get a portal app by app ID",
        response_model=List[ClosedAppView],
        tags=["mps-mock-customer"],
    )
    async def _(
        query: CustomerAppViewQuery,
        organization_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        app_views = _query_app_views(query=query)
        return app_views

    @app.get(
        "/customer/apps/{app_id}",
        summary="Get an app by ID",
        response_model=ClosedApp,
        tags=["mps-mock-customer"],
    )
    async def _(
        app_id: str,
        organization_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        _, _, app = _get_app(app_id=app_id)
        return app

    @app.get(
        "/customer/apps/{app_id}/config",
        summary="Get the configuration of an existing app",
        response_model=AppConfiguration,
        tags=["mps-mock-customer"],
    )
    async def _(
        app_id: str,
        organization_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        # check if app exists
        _, _, _ = _get_app(app_id=app_id)
        config = {"app_id": app_id, "global": {}, "customer": {}}
        if str(app_id) in APP_V1_CONFIGURATIONS_BY_ID:
            raw_config = APP_V1_CONFIGURATIONS_BY_ID[app_id]
            if "global" in raw_config:
                config["global"] = raw_config["global"]
            if "customer" in raw_config and organization_id in raw_config["customer"]:
                config["customer"] = raw_config["customer"][organization_id]
        return AppConfiguration.model_validate(config)

    @app.put(
        "/custom-mock/apps/{app_id}/config/global",
        summary="Add or update the global configuration of an existing app",
        tags=["mps-custom-mock"],
    )
    async def _(
        config: ConfigurationModel,
        app_id: str,
        payload=api_integration.global_depends(),
    ):
        active_api_version, _, app = _get_app(app_id=app_id)
        _validate_global_config(active_api_version, app, config)

        if app_id in APP_V1_CONFIGURATIONS_BY_ID:
            APP_V1_CONFIGURATIONS_BY_ID[app_id].update({"global": config})
        else:
            APP_V1_CONFIGURATIONS_BY_ID[app_id] = {"global": config}
        with configurations_v1_file_path.open("w") as f:
            json.dump(APP_V1_CONFIGURATIONS_BY_ID, f, default=str)
        return

    @app.put(
        "/custom-mock/apps/{app_id}/config/customer/{customer_id}",
        summary="Add or update the customer configuration of an existing app",
        tags=["mps-custom-mock"],
    )
    async def _(
        config: ConfigurationModel,
        app_id: str,
        customer_id: str,
        payload=api_integration.global_depends(),
    ):
        active_api_version, _, app = _get_app(app_id=app_id)
        _validate_customer_config(active_api_version, app, config)

        if app_id in APP_V1_CONFIGURATIONS_BY_ID:
            existing_config = APP_V1_CONFIGURATIONS_BY_ID[app_id]
            if "customer" in config:
                existing_config["customer"].update({customer_id: config})
            else:
                existing_config["customer"] = {customer_id: config}
            APP_V1_CONFIGURATIONS_BY_ID[app_id].update(existing_config)
        else:
            APP_V1_CONFIGURATIONS_BY_ID[app_id] = {"customer": {customer_id: config}}

        with configurations_v1_file_path.open("w") as f:
            json.dump(APP_V1_CONFIGURATIONS_BY_ID, f, default=str)
        return

    @app.get(
        "/customer/apps/{app_id}/app-ui-config",
        summary="Get the app-ui configuration of an existing app",
        response_model=AppUiConfiguration,
        tags=["mps-mock-customer"],
    )
    async def _(
        app_id: str,
        organization_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        if str(app_id) in APP_V1_APP_UI_CONFIGURATION_BY_ID:
            return APP_V1_APP_UI_CONFIGURATION_BY_ID[app_id]
        raise HTTPException(status_code=404, detail="App-UI configuration does not exist")

    @app.put(
        "/custom-mock/apps/{app_id}/app-ui-config",
        summary="Add or update the app-ui configuration of an existing app",
        tags=["mps-custom-mock"],
    )
    async def _(
        app_ui_config: AppUiConfiguration,
        app_id: str,
        payload=api_integration.global_depends(),
    ):
        ui_config_dict = AppUiConfiguration.model_dump(app_ui_config)
        if str(app_id) in APP_V1_APP_UI_CONFIGURATION_BY_ID:
            APP_V1_APP_UI_CONFIGURATION_BY_ID[app_id].update(ui_config_dict)
        else:
            APP_V1_APP_UI_CONFIGURATION_BY_ID[app_id] = ui_config_dict
        # logger.info(APP_V1_APP_UI_CONFIGURATION_BY_ID)
        # return APP_V1_APP_UI_CONFIGURATION_BY_ID
        with app_ui_configurations_v1_file_path.open("w") as f:
            json.dump(APP_V1_APP_UI_CONFIGURATION_BY_ID, f, default=str)
        return

    @app.get(
        "/compute/organizations/{organization_id}/apps/{app_id}",
        summary="Get an app by ID",
        response_model=ClosedApp,
        tags=["mps-mock-compute"],
    )
    async def _(
        organization_id: str,
        app_id: str,
        payload=api_integration.global_depends(),
    ):
        _, _, app = _get_app(app_id=app_id)
        return app

    def _query_app_views(query: CustomerAppViewQuery) -> List[ClosedAppView]:
        app_ids = [str(app_id) for app_id in query.apps]
        list_app_views = []
        for portal_app_id in APPS_V1_BY_ID:
            portal_app = APPS_V1_BY_ID[portal_app_id]
            for active_api_version in portal_app["active_app_views"]:
                if query.api_versions and active_api_version not in query.api_versions:
                    continue
                active_app = portal_app["active_app_views"][active_api_version]
                if active_app and str(active_app["app"]["id"]) in app_ids:
                    list_app_views.append(active_app)
        return list_app_views

    def _get_app_view(app_id: str) -> ClosedAppView:
        for portal_app_id in APPS_V1_BY_ID:
            portal_app = APPS_V1_BY_ID[portal_app_id]
            for active_api_version in portal_app["active_app_views"]:
                active_app = portal_app["active_app_views"][active_api_version]
                if active_app and str(active_app["app"]["id"]) == str(app_id):
                    app_view = ClosedAppView(**active_app)
                    return active_api_version, portal_app_id, app_view

        raise HTTPException(status_code=404, detail="App view does not exist")

    def _get_portal_app(portal_app_id: str) -> ClosedPortalApp:
        if portal_app_id in APPS_V1_BY_ID:
            return APPS_V1_BY_ID[portal_app_id]
        raise HTTPException(status_code=404, detail="Portal app does not exist")

    def _get_app(app_id: str):
        active_app_version, portal_app_id, app_view = _get_app_view(app_id)
        if not app_view.app:
            raise HTTPException(status_code=404, detail="App does not exist")
        return active_app_version, portal_app_id, app_view.app

    def _validate_global_config(active_api_version: str, app: ClosedApp, app_config: dict):
        try:
            if active_api_version.lower() in ["v1", "v2"]:
                ead_validator_legacy.validate_global_config(app.ead, app_config)
            else:
                ead_validator.validate_global_config(app.ead, app_config)
        except ConfigValidationError as e:
            error = f"ConfigValidationError: {e}."
            raise HTTPException(status_code=422, detail=error) from e

    def _validate_customer_config(active_api_version: str, app: ClosedApp, app_config: dict):
        try:
            if active_api_version.lower() in ["v1", "v2"]:
                ead_validator_legacy.validate_customer_config(app.ead, app_config)
            else:
                ead_validator.validate_customer_config(app.ead, app_config)
        except ConfigValidationError as e:
            error = f"ConfigValidationError: {e}."
            raise HTTPException(status_code=422, detail=error) from e

    def _validate_ead(active_app_version: str, ead: dict) -> str:
        try:
            if active_app_version.lower() in ["v1", "v2"]:
                ead_validator_legacy.validate(ead)
                version = ead_validator_legacy.get_namespace_version(ead)
            else:
                ead_validator.validate(ead)
                version = ead_validator.get_namespace_version(ead)
            return version
        except EadSchemaValidationError as e:
            error = f"EadSchemaValidationError: {e}."
            raise HTTPException(status_code=422, detail=error) from e
        except EadValidationError as e:
            error = f"EadValidationError: {e}"
            raise HTTPException(status_code=422, detail=error) from e
