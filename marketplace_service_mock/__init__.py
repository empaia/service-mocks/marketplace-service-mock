from importlib.metadata import version

__version__ = version("marketplace-service-mock")
