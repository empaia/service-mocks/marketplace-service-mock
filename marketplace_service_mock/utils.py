import os
from collections import defaultdict
from csv import DictReader
from typing import List

from marketplace_service_mock.models.marketplace.app import AppTag, Language, TagList, TextTranslation

tag_definitions = "definitions/tags/tags.csv"


def generate_tags_model(app_tag_dict: dict) -> TagList:
    tags = TagList()
    for group_name in app_tag_dict:
        # note: refactor when MPS supports all tag groups for apps and move ExtendedTagList to models repo
        if group_name in ["TISSUE", "STAIN", "ANALYSIS", "INDICATION", "CLEARANCE"]:
            group_tags = app_tag_dict[group_name]
            for tag_name in group_tags:
                tag_translations = translate_dict(group_tags[tag_name])
                app_tag = AppTag(name=tag_name, tag_translations=tag_translations)
                getattr(tags, f"{str(group_name).lower().removesuffix('s')}s").append(app_tag)
    return tags


def translate_dict(translation_dict: dict) -> List[TextTranslation]:
    translation = []
    for tag_abbr in translation_dict:
        uppper_tag_abbr = str(tag_abbr).upper()
        if uppper_tag_abbr in Language.__members__.values():
            translation.append(TextTranslation(lang=Language(uppper_tag_abbr), text=translation_dict[tag_abbr]))
        else:
            raise ValueError(f"Invalid tag abbreviation ({uppper_tag_abbr})")
    return translation


def read_tag_csv_to_dict() -> dict:
    filename = os.path.join(os.path.dirname(__file__), tag_definitions)
    tag_group_dict = defaultdict(dict)
    for record in DictReader(open(filename, encoding="utf-8"), delimiter=";"):
        if "TAG_NAME_ID" in record and "TAG_GROUP" in record:
            tag_translation = dict(list(record.items())[2:])
            tag_group_dict[record["TAG_GROUP"]][record["TAG_NAME_ID"]] = tag_translation
    return tag_group_dict
