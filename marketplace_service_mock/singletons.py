import json
from logging import DEBUG, getLogger
from pathlib import Path

from marketplace_service_mock.utils import generate_tags_model, read_tag_csv_to_dict

from .api_integrations import get_api_integration
from .py_ead_validation.py_ead_validation.ead_validator import create_validator
from .settings import Settings

logger = getLogger("uvicorn")
settings = Settings()

if settings.debug:
    logger.level = DEBUG

api_integration = get_api_integration(settings, logger)

Path(settings.storage_dir_path).mkdir(parents=True, exist_ok=True)

app_tags_dict = read_tag_csv_to_dict()
app_tags_model = generate_tags_model(app_tags_dict)

apps_v1_file_path = Path(settings.storage_dir_path, "apps_v1.json")
configurations_v1_file_path = Path(settings.storage_dir_path, "configuration_v1.json")
app_ui_configurations_v1_file_path = Path(settings.storage_dir_path, "app_ui_configuration_v1.json")

ead_validator = create_validator(enable_legacy_support=False)
ead_validator_legacy = create_validator(enable_legacy_support=True)

APPS_V1_BY_ID = {}
APP_V1_CONFIGURATIONS_BY_ID = {}
APP_V1_APP_UI_CONFIGURATION_BY_ID = {}

try:
    with apps_v1_file_path.open() as f:
        try:
            APPS_V1_BY_ID = json.load(f)
        except json.JSONDecodeError:
            json.dump({}, f)
except FileNotFoundError:
    pass

try:
    with configurations_v1_file_path.open() as f:
        try:
            APP_V1_CONFIGURATIONS_BY_ID = json.load(f)
        except json.JSONDecodeError:
            json.dump({}, f)
except FileNotFoundError:
    pass

try:
    with app_ui_configurations_v1_file_path.open() as f:
        try:
            APP_V1_APP_UI_CONFIGURATION_BY_ID = json.load(f)
        except json.JSONDecodeError:
            json.dump({}, f)
except FileNotFoundError:
    pass
