from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    debug: bool = False
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    api_integration: str = ""
    root_path: str = ""
    disable_openapi: bool = False
    storage_dir_path: str = "/data"

    model_config = SettingsConfigDict(env_file=".env", env_prefix="mpsm_", extra="ignore")
